export default function ({ $axios, store, redirect }) {
  $axios.onRequest((config) => {
    config.headers.common['Content-Type'] = 'application/json'
    config.headers.common['Accept'] = 'application/json'
    // config.headers.common['Access-Control-Allow-Origin'] = '*'

    if (store.state.user.token) {
      $axios.setToken(store.state.user.token, 'Bearer')
    } else {
      $axios.setToken(false)
    }
  })
}
