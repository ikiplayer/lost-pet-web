import Vue from 'vue'
import VueTypeAheadBootstrap from 'vue-typeahead-bootstrap'
import 'bootstrap/scss/bootstrap.scss'

Vue.component('v-autocomplete', VueTypeAheadBootstrap)

// TODO: USE DEBOUNCE TO DELAY API CALL