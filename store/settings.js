const _ = require('lodash')

export const state = () => ({
  ui: true,
  state: {
    layout: {
      layout: 'fixed',
      rtl: false,
    },
    'app::mainDrawer': {
      align: 'start',
      sidebar: 'dark',
    },
    'fixed::mainDrawer': {
      align: 'start',
      sidebar: 'dark',
    },
    'sticky::mainDrawer': {
      align: 'start',
      sidebar: 'dark',
    },
    'boxed::mainDrawer': {
      align: 'start',
      sidebar: 'light',
    },
    'app::mainNavbar': {
      navbar: 'light',
    },
    'fixed::mainNavbar': {
      navbar: 'light',
    },
    'sticky::mainNavbar': {
      navbar: 'light',
    },
    'boxed::mainNavbar': {
      navbar: 'light',
    },
  },
})

export const mutations = {
  SET_SETTINGS(state, settings) {
    if (settings) {
      state.state = _.merge({}, state.state, settings)
    }
  },
}

export const actions = {
  setSettings({ commit }, settings) {
    commit('SET_SETTINGS', settings)
  },
}

export const getters = {
  layout: (state) => state.state.layout.layout,
  routes: (state, getters) => {
    const layout = getters.layout
    return {
      billingHistory: {
        name: 'layout-billing-history',
        params: { layout },
      },
      billingInvoice: {
        name: 'layout-billing-invoice',
        params: { layout },
      },
      billingPayment: {
        name: 'layout-billing-payment',
        params: { layout },
      },
      billing: {
        name: 'layout-billing',
        params: { layout },
      },
      changePassword: {
        name: 'layout-change-password',
        params: { layout },
      },
      discussion: {
        name: 'layout-discussion',
        params: { layout },
      },
      editAccountProfile: {
        name: 'layout-edit-account-profile',
        params: { layout },
      },
      forgotPassword: {
        name: 'layout-forgot-password',
        params: { layout },
      },
      home: {
        name: 'layout-home',
      },
      instructorEditCourse: {
        name: 'layout-instructor-edit-course',
        params: { layout },
      },
      login: {
        name: 'layout-login',
        params: { layout },
      },
      signup: {
        name: 'layout-signup',
        params: { layout },
      },
      studentProfile: {
        name: 'layout-student-profile',
        params: { layout },
      },
      studentQuizResult: {
        name: 'layout-student-quiz-result',
        params: { layout },
      },
    }
  },
}
