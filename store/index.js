export const state = () => ({
  brand: 'Lost Dogge',
  docsHostname: '',
  user: localStorage.getItem('user')
    ? JSON.parse(localStorage.getItem('user'))
    : {},
  // WHAT NEEDS TO STORE

  filter: {
    // STORE IN LOCAL
    address: null,
    city: null,
    state: null,
    country: null,
    zipcode: null,
    latitude: null,
    longitude: null,
    animal_types: localStorage.getItem('filterAnimalTypes')
      ? JSON.parse(localStorage.getItem('filterAnimalTypes'))
      : [],
    //
    breed: sessionStorage.getItem('filterBreed')
      ? JSON.parse(sessionStorage.getItem('filterBreed'))
      : null,
    color_types: sessionStorage.getItem('filterColorTypes')
      ? JSON.parse(sessionStorage.getItem('filterColorTypes'))
      : [],
    coat_types: sessionStorage.getItem('filterCoatTypes')
      ? JSON.parse(sessionStorage.getItem('filterCoatTypes'))
      : [],
    ages: sessionStorage.getItem('filterAges')
      ? JSON.parse(sessionStorage.getItem('filterAges'))
      : [],
  },
  ages: [
    { id: 1, name: 'Young' },
    { id: 3, name: 'Adult' },
  ],
  unitMeasurements: [
    { id: 0, name: 'Imperial' },
    { id: 1, name: 'Metric' },
  ],
  breeds: localStorage.getItem('breeds')
    ? JSON.parse(localStorage.getItem('breeds'))
    : [],
  animalTypes: localStorage.getItem('animalTypes')
    ? JSON.parse(localStorage.getItem('animalTypes'))
    : [],
  colors: localStorage.getItem('colors')
    ? JSON.parse(localStorage.getItem('colors'))
    : {},
  coatTypes: localStorage.getItem('coatTypes')
    ? JSON.parse(localStorage.getItem('coatTypes'))
    : {},
})

export const getters = {
  settings: (state) => state.settings.state,
  userToken: (state) => {
    return state.user.token
  },
  breedByFilterAnimalType: (state) => {
    if (state.filter.animal_type) {
      return state.breed.filter((item) => {
        return item.animal_type_id == state.filter.animal_type
      })
    } else {
      return state.breeds
    }
  },
  // catBreeds: (state) => {
  //   return state.breeds.filter((item) => {
  //     return item.animal_type_id == 2
  //   })
  // },
  // dogBreeds: (state) => {
  //   return state.breeds.filter((item) => {
  //     return item.animal_type_id == 1
  //   })
  // },
}

export const actions = {
  updateUser({ commit }, payload) {
    commit('UPDATE_USER', payload)
    if (!payload.token) return
    this.$axios.setToken(payload.token, 'Bearer')
  },
  updateFilter({ commit }, payload) {
    commit('UPDATE_FILTER', payload)
  },
  updateBreeds({ commit }, payload) {
    commit('UPDATE_BREEDS', payload)
  },
  updateColors({ commit }, payload) {
    commit('UPDATE_COLORS', payload)
  },
  updateAnimalTypes({ commit }, payload) {
    commit('UPDATE_ANIMAL_TYPES', payload)
  },
  updateCoatTypes({ commit }, payload) {
    commit('UPDATE_COAT_TYPES', payload)
  },
  updateFilterPetTypes({ commit }, payload) {
    commit('UPDATE_FILTER_PET_TYPES', payload)
  },
  updateFilterBreedType({ commit }, payload) {
    commit('UPDATE_FILTER_BREED_TYPE', payload)
  },
  updateFilterColorTypes({ commit }, payload) {
    commit('UPDATE_FILTER_COLOR_TYPES', payload)
  },
  updateFilterCoatTypes({ commit }, payload) {
    commit('UPDATE_FILTER_COAT_TYPES', payload)
  },
  updateFilterAgeTypes({ commit }, payload) {
    commit('UPDATE_FILTER_AGE_TYPES', payload)
  },
  clearFilter({ commit }, payload) {
    commit('CLEAR_FILTER', payload)
  },
  logout({ commit }, payload) {
    commit('LOGOUT', payload)
  },
}

export const mutations = {
  // Updates user info in state and localstorage
  UPDATE_USER(state, payload) {
    // Get Data localStorage
    let user = JSON.parse(localStorage.getItem('user')) || state.user

    for (const property of Object.keys(payload)) {
      if (payload[property] != null) {
        user[property] = payload[property]
      }
    }
    // Store data in localStorage
    localStorage.setItem('user', JSON.stringify(user))
  },
  UPDATE_FILTER() {
    for (const property of Object.keys(payload)) {
      if (payload[property] != null) {
        user[property] = payload[property]
      }
    }
  },
  UPDATE_BREEDS(state, payload) {
    state.breeds = payload
    localStorage.setItem('breeds', JSON.stringify(payload))
  },
  UPDATE_COLORS(state, payload) {
    state.colors = payload
    localStorage.setItem('colors', JSON.stringify(payload))
  },
  UPDATE_ANIMAL_TYPES(state, payload) {
    state.animalTypes = payload
    localStorage.setItem('animalTypes', JSON.stringify(payload))
  },
  UPDATE_COAT_TYPES(state, payload) {
    state.coatTypes = payload
    localStorage.setItem('coatTypes', JSON.stringify(payload))
  },
  UPDATE_FILTER_PET_TYPES(state, payload) {
    state.filter.animal_types = payload
    localStorage.setItem('filterAnimalTypes', JSON.stringify(payload))
  },
  UPDATE_FILTER_BREED_TYPE(state, payload) {
    state.filter.breed = payload
    sessionStorage.setItem('filterBreed', JSON.stringify(payload))
  },
  UPDATE_FILTER_COLOR_TYPES(state, payload) {
    state.filter.color_types = payload
    sessionStorage.setItem('filterColorTypes', JSON.stringify(payload))
  },
  UPDATE_FILTER_COAT_TYPES(state, payload) {
    state.filter.coat_types = payload
    sessionStorage.setItem('filterCoatTypes', JSON.stringify(payload))
  },
  UPDATE_FILTER_AGE_TYPES(state, payload) {
    state.filter.ages = payload
    sessionStorage.setItem('filterAges', JSON.stringify(payload))
  },
  CLEAR_FILTER(state, payload) {
    localStorage.removeItem('filterAnimalTypes')
    sessionStorage.removeItem('filterBreed')
    sessionStorage.removeItem('filterColorTypes')
    sessionStorage.removeItem('filterCoatTypes')
    sessionStorage.removeItem('filterAges')

    state.filter = {
      animalTypes: [],
      breed: null,
      color_types: [],
      coat_types: [],
      ages: [],
    }
  },
  LOGOUT(state, payload) {
    state.user = {}
    localStorage.removeItem('user')
    this.$axios.setToken(false)
  },
}
